﻿using UnityEngine;
using UnityEngine.UI;

public class PanelController : MonoBehaviour
{
    #region PRIVATE VARIBLES

    [SerializeField] private Image Panel_1 = null;
    [SerializeField] private Image Panel_2 = null;
    [SerializeField] private Image Panel_News = null;

    [SerializeField] private float panelYpos = 0f;
    
    private Vector3 PanelStartPosition;
    private Vector3 PanelEndPosition;

    private bool Panel_1IsUp = false;
    private bool Panel_2IsUp = false;
    private bool Panel_NewsIsUp = false;

    #endregion 

    void Start()
    {
        PanelEndPosition = Panel_1.rectTransform.position;
        PanelStartPosition = Panel_1.rectTransform.position + new Vector3(0f, panelYpos, 0f);
        Panel_1.rectTransform.transform.position = PanelStartPosition;
        Panel_2.rectTransform.transform.position = PanelStartPosition;
        Panel_News.rectTransform.transform.position = PanelStartPosition;
    }

    void Update()
    {
        PopPanelsUpDown(Panel_1, Panel_2, Panel_News);
    }

    public void SwitchPanel()
    {
        if (!Panel_1IsUp)
        {
            Panel_1IsUp = true;
        }
        else
        {
            Panel_1IsUp = false;
        }
    }
    public void SwitchPanel_2()
    {
        if (!Panel_2IsUp)
        {
            Panel_2IsUp = true;
        }
        else
        {
            Panel_2IsUp = false;
        }
    }
    public void SwitchNews()
    {
        if (!Panel_NewsIsUp)
        {
            Panel_NewsIsUp = true;
        }
        else
        {
            Panel_NewsIsUp = false;
        }
    }


    private void PopPanelsUpDown(Image panel, Image panel2, Image panelnews)
    {
        if (Panel_1IsUp)
        {
            panel.rectTransform.position = Vector3.Lerp(PanelStartPosition, PanelEndPosition, 1f);
        }
        else
        {
            panel.rectTransform.position = Vector3.Lerp(PanelEndPosition, PanelStartPosition, 1f);
        }

        if (Panel_2IsUp)
        {
            panel2.rectTransform.position = Vector3.Lerp(PanelStartPosition, PanelEndPosition, 1f);
        }
        else
        {
            panel2.rectTransform.position = Vector3.Lerp(PanelEndPosition, PanelStartPosition, 1f);
        }

        if (Panel_NewsIsUp)
        {
            panelnews.rectTransform.position = Vector3.Lerp(PanelStartPosition, PanelEndPosition, 1f);
        }
        else
        {
            panelnews.rectTransform.position = Vector3.Lerp(PanelEndPosition, PanelStartPosition, 1f);
        }
    }

   
}
