﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Ragebar : MonoBehaviour
{
    public Slider slider;
    public TextMeshProUGUI ragebar;

    int rage;

    private void Start()
    {
        slider = GetComponent<Slider>();
    }

    private void Update()
    {
        if (rage < 0)
        {
            rage = 0;
        }
    }

    public void SetMaxRage(int rage)
    {
        slider.maxValue = rage;
        slider.value = rage;
        ragebar.SetText(rage.ToString());
    }

    public void SetRage(int rage)
    {
        string txt;
        slider.value += rage;
        txt = slider.value.ToString();
        ragebar.SetText(txt);
    }



}
