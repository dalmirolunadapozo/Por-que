﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    public static float timer;
    public static bool timeStarted = false;

    [SerializeField] private TextMeshProUGUI timerUI = null;
   
    [SerializeField] private float timeRemaining = 10;

    private void Start()
    {
        timeStarted = true;
    }

    void Update()
    {
        if (timeStarted)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
        }
        else
        {
            timeRemaining = 0;
            timeStarted = false;
        }
    
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timerUI.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

}
